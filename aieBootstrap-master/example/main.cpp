#include "exampleApp.h"

int main() {
	
	auto app = new exampleApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}