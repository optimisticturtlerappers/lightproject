#include "mirrors.h"
#include "Input.h"
#include "ShivsGameApp.h"


mirrors::mirrors(ShivsGameApp* myGame)
{
	mirrorPosX = -100;
	mirrorPosY = -100;
	//default sets them well off screen
	game = myGame;
	isActive = false;
	gameHasPaused = true;
	mirrorRotation = 0;
}


mirrors::~mirrors()
{
}

void mirrors::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();


	////last ver. obsoleted
	////while mouse button is pressed
	//if (input->isMouseButtonDown)
	//{
	//	if (hasMouseClick() && !gameHasPaused)
	//		isActive = true;
	//}


	////move mirror with the mouse
	//if (isActive && !gameHasPaused)
	//{
	//	mirrorPosX = input->getMouseX();
	//	mirrorPosY = input->getMouseY();
	//}

	////release mirror when mouse button is relased
	//if (input->isMouseButtonUp)
	//{
	//	if (isActive && !gameHasPaused)
	//		isActive = false;
	//}

	////flip selected mirror on side if left shift is pressed
	//if (input->isKeyDown(aie::INPUT_KEY_LEFT_SHIFT))
	//{
	//	if (isActive && !gameHasPaused)
	//	{
	//		if (isVertical)
	//		{
	//			isVertical = false;
	//			mirrorRotation = 0;
	//		}
	//		else
	//		{
	//			isVertical = true;
	//			mirrorRotation = 0.5*PI;
	//		}
	//	}
	//}
}

void mirrors::draw(aie::Renderer2D * renderer)
{
	//renderer->drawBox(mirrorPosX, mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirrorRotation);
}

void mirrors::setStartPaused()
{
	if (gameHasPaused)
		gameHasPaused = false;
	else
		gameHasPaused = true;
}

//bool mirrors::hasMouseClick()
//{
//	aie::Input* input = aie::Input::getInstance();
//
//	int leftSide = mirrorPosX - MIRROR_WIDTH / 2;
//	int rightSide = mirrorPosX + MIRROR_WIDTH / 2;
//	int topSide = mirrorPosY + MIRROR_HEIGHT / 2;
//	int bottomSide = mirrorPosY - MIRROR_HEIGHT / 2;
//
//	//mouse is detected with the borders of the mirror, return true
//	if (input->getMouseX() >= leftSide && input->getMouseX() <= rightSide)
//		if (input->getMouseY() >= bottomSide && input->getMouseY() <= topSide)
//			return true;
//
//	return false;
//}
