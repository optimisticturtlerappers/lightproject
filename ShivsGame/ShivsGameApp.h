#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "mirrors.h"
#include "Audio.h"

#define PADDLE_SPEED 1000
#define BALL_SPEED 300;

#define BALL_RADIUS 20

#define MAX_MIRRORS 4
#define MAX_SPARKLES 8
#define SPARKLE_SIZE 1

enum Direction {NONE, NORTH, SOUTH, EAST, WEST};

struct sparkle {
	int x;
	int y;
	bool isActive;
};

struct box {
	int x;
	int y;
};

class ShivsGameApp : public aie::Application 
{
public:

	ShivsGameApp();
	virtual ~ShivsGameApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	Direction ballCollideRectangle(int rCentreX, int rCentreY, int rWidth, int rHeight, float rRotation);
	void reflecBall(Direction colDirection, bool hitPaddle);

	int paddlePosX;
	int paddlePosY;


	int ballSpeed;
	int ballX;
	int ballY;
	int lastBallX, lastBallY;
	int ballSpeedX;
	int ballSpeedY;
	int lives;
	float ballDirection;
	
	int sparkleCount;

	bool isGameOver;
	//pause at start of game
	bool stageStarted;

	bool hasMultiButtonPressed();

	aie::Texture*	m_lightTexture;
	aie::Texture*	m_mirrorsTexture;
	aie::Texture*	m_mainMenuTexture;
	aie::Texture*	m_controlsTexture;
	aie::Texture*	m_backgroundTexture;
	aie::Texture*	m_creditsTexture;
	
	aie::Audio*		m_bounceaudio;
	aie::Audio*		m_audio1;
	aie::Audio*		m_audio2;
	aie::Audio*		m_audio3;
	aie::Audio*		m_loselifeaudio;

	int pickedmusic;

	bool controlsDismissed;
	bool gameStarted;
	bool controlsRaised;

	//array of mirrors in case want to use multiple per stage
	mirrors* mirror[MAX_MIRRORS];

	//array of sparkles
	sparkle trail[MAX_SPARKLES];
};