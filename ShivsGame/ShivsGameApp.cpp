#include "ShivsGameApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <random>
#include <time.h>
#include <cmath>

using std::abs;

ShivsGameApp::ShivsGameApp() 
{

}

ShivsGameApp::~ShivsGameApp() 
{

}

bool ShivsGameApp::startup() 
{
	
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 32);

	m_lightTexture = new aie::Texture("./textures/LightLeftNEW.png");
	m_audio1 = new aie::Audio("./audio/8bitscrap.wav");
	m_audio2 = new aie::Audio("./audio/8bitspacegroove.wav");
	m_audio3 = new aie::Audio("./audio/gunslinger.wav");
	m_bounceaudio = new aie::Audio("./audio/bounce sound effect.wav");
	m_loselifeaudio = new aie::Audio("./audio/lose life sound.wav");
	m_mirrorsTexture = new aie::Texture("./textures/mirrors.png");
	m_mainMenuTexture = new aie::Texture("./textures/main menu.png");
	m_controlsTexture = new aie::Texture("./textures/controls new colour.png");
	m_backgroundTexture = new aie::Texture("./textures/new background.png");
	m_creditsTexture = new aie::Texture("./textures/new credits.png");
	//initialise null music selection
	pickedmusic = -1;

	ballSpeed = BALL_SPEED;
	mirror[0] = new mirrors(this);
	mirror[0]->mirrorPosX = getWindowWidth() / 2;
	mirror[0]->mirrorPosY = 0;

	mirror[1] = new mirrors(this);
	mirror[1]->mirrorPosX = getWindowWidth() / 2;
	mirror[1]->mirrorPosY = getWindowHeight();

	mirror[2] = new mirrors(this);
	mirror[2]->mirrorPosX = 0;
	mirror[2]->mirrorPosY = getWindowHeight() / 2 ;
	mirror[2]->mirrorRotation = 0.5*PI;

	mirror[3] = new mirrors(this);
	mirror[3]->mirrorPosX = getWindowWidth();
	mirror[3]->mirrorPosY = getWindowHeight() / 2;
	mirror[3]->mirrorRotation = 0.5*PI;

	ballX = getWindowWidth() / 2;
	ballY = getWindowHeight() - 150;
	ballDirection = 0; 
	ballSpeedX = 0;
	ballSpeedY = 0; //freeze ball at start of game
	lives = 9;

	//deactivates trail at game start
	for (int i = 0; i < MAX_SPARKLES; i++)
	{
		trail[i].isActive = false;
		trail[i].x = -10;
		trail[i].y = -10;
	}
	sparkleCount = 0;

	gameStarted = false;
	controlsRaised = false;
	isGameOver = false;
	stageStarted = false;
	controlsDismissed = false;

	return true;
}

void ShivsGameApp::shutdown()
{
	delete m_audio1;
	delete m_audio2;
	delete m_audio3;
	delete m_loselifeaudio;
	delete m_mirrorsTexture;
	delete m_mainMenuTexture;
	delete m_controlsTexture;

	delete m_font;
	delete m_lightTexture;
	delete m_2dRenderer;
}

void ShivsGameApp::update(float deltaTime)
{

	// input example
	aie::Input* input = aie::Input::getInstance();

	if (!gameStarted && !controlsRaised)
	{
		if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_LEFT))
		{
			int mouseX = input->getMouseX();
			int mouseY = input->getMouseY();
			if (mouseX >= 100 && mouseX <= 590 && mouseY >= (getWindowHeight()-210) && mouseY <= (getWindowHeight()-30))
				gameStarted = true;
			if (mouseX >= 100 && mouseX <= 590 && mouseY >= (getWindowHeight() - 450) && mouseY <= (getWindowHeight() - 270))
				controlsRaised = true;
			if (mouseX >= 100 && mouseX <= 590 && mouseY >= (getWindowHeight() - 690) && mouseY <= (getWindowHeight() - 480))
			{
				if (controlsDismissed)
					controlsDismissed = false;
				else
					quit();
			}
		}
	}

	if (!gameStarted && controlsRaised)
	{
		if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_LEFT))
		{
			int mouseX = input->getMouseX();
			int mouseY = input->getMouseY();
			if (mouseX >= 210 && mouseX <= 500 && mouseY >= (getWindowHeight() - 670) && mouseY <= (getWindowHeight() - 550))
			{
				controlsRaised = false;
				controlsDismissed = true;
			}
		}
	}

	//generate new random seed
	if (gameStarted)
	{
		srand(time(NULL));

		int random;

		// exit the application
		if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
			quit();

		if (input->isKeyDown(aie::INPUT_KEY_SPACE))
		{
			if (!stageStarted)
			{
				stageStarted = true;
				//random starting direction
				random = rand() % 4;
				switch (random)
				{
				case 0:
					ballSpeedX = 0;
					ballSpeedY = -ballSpeed;
					ballDirection = 0.5*PI;
					break;
				case 1:
					ballSpeedX = 0;
					ballSpeedY = ballSpeed;
					ballDirection = -0.5*PI;
					break;
				case 2:
					ballSpeedX = -ballSpeed;
					ballSpeedY = 0;
					ballDirection = 0;
					break;
				case 3:
					ballSpeedX = ballSpeed;
					ballSpeedY = 0;
					ballDirection = PI;
					break;
				}
			}
		}

		//if not picked, pick a music
		if (pickedmusic == -1)
			pickedmusic = rand() % 3;

		switch (pickedmusic)
		{
		case 0:
			m_audio1->play();
			break;
		case 1:
			m_audio2->play();
			break;
		case 2:
			m_audio3->play();
			break;
		}

		lastBallX = ballX;
		lastBallY = ballY;

		Direction colResult;

		//move ball based on its current speed
		ballX += ballSpeedX * deltaTime;
		ballY += ballSpeedY * deltaTime;

		//trail placement
		//activates trail only if game has started
		if (stageStarted)
		{
			trail[sparkleCount].x = lastBallX;
			trail[sparkleCount].y = lastBallY;
			if (!trail[sparkleCount].isActive)
				trail[sparkleCount].isActive = true;
			sparkleCount++;
			if (sparkleCount >= MAX_SPARKLES)
				sparkleCount = 0;
		}

		colResult = ballCollideRectangle(mirror[0]->mirrorPosX, mirror[0]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[0]->mirrorRotation);

		if (colResult != Direction::NONE)
		{
			if (input->isKeyUp(aie::INPUT_KEY_S) || hasMultiButtonPressed())
			{
				lives--;
				m_loselifeaudio->play();
			}
			m_bounceaudio->play();
			ballSpeed += 10;
			random = rand() % 3;
			switch (random)
			{
				//goes to top 
			case 0:
				ballSpeedX = 0;
				ballSpeedY = ballSpeed;
				ballDirection = -0.5*PI;
				break;
				//goes to left
			case 1:
				ballSpeedX = ballSpeed;
				ballSpeedY = ballSpeed;
				ballDirection = -0.75*PI;
				break;
				//goes to right
			case 2:
				ballSpeedX = -ballSpeed;
				ballSpeedY = ballSpeed;
				ballDirection = -0.25*PI;
				break;
			}
			//reflecBall(colResult,false);
		}


		colResult = ballCollideRectangle(mirror[1]->mirrorPosX, mirror[1]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[0]->mirrorRotation);

		if (colResult != Direction::NONE)
		{
			if (input->isKeyUp(aie::INPUT_KEY_W) || hasMultiButtonPressed())
			{
				lives--;
				m_loselifeaudio->play();
			}
			m_bounceaudio->play();
			ballSpeed += 10;
			random = rand() % 3;
			switch (random)
			{
				//goes to bottom 
			case 0:
				ballSpeedX = 0;
				ballSpeedY = -ballSpeed;
				ballDirection = 0.5*PI;
				break;
				//goes to right
			case 1:
				ballSpeedX = ballSpeed;
				ballSpeedY = -ballSpeed;
				ballDirection = 0.75*PI;
				break;
				//goes to left
			case 2:
				ballSpeedX = -ballSpeed;
				ballSpeedY = -ballSpeed;
				ballDirection = 0.25*PI;
				break;
			}
			//reflecBall(colResult,false);
		}

		colResult = ballCollideRectangle(mirror[2]->mirrorPosX, mirror[2]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[2]->mirrorRotation);

		if (colResult != Direction::NONE)
		{
			if (input->isKeyUp(aie::INPUT_KEY_A) || hasMultiButtonPressed())
			{
				lives--;
				m_loselifeaudio->play();
			}
			m_bounceaudio->play();
			ballSpeed += 10;
			random = rand() % 3;
			switch (random)
			{
			case 0:
				ballSpeedX = ballSpeed;
				ballSpeedY = 0;
				ballDirection = PI;
				break;
			case 1:
				ballSpeedX = ballSpeed;
				ballSpeedY = ballSpeed;
				ballDirection = -0.75*PI;
				break;
			case 2:
				ballSpeedX = ballSpeed;
				ballSpeedY = -ballSpeed;
				ballDirection = 0.75*PI;
				break;
			}
			//reflecBall(colResult, false);
		}


		colResult = ballCollideRectangle(mirror[3]->mirrorPosX, mirror[3]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[3]->mirrorRotation);

		if (colResult != Direction::NONE)
		{
			if (input->isKeyUp(aie::INPUT_KEY_D) || hasMultiButtonPressed())
			{
				lives--;
				m_loselifeaudio->play();
			}
			m_bounceaudio->play();
			ballSpeed += 10;
			random = rand() % 3;
			switch (random)
			{
			case 0:
				ballSpeedX = -ballSpeed;
				ballSpeedY = 0;
				ballDirection = 0;
				break;
			case 1:
				ballSpeedX = -ballSpeed;
				ballSpeedY = ballSpeed;
				ballDirection = -0.25*PI;
				break;
			case 2:
				ballSpeedX = -ballSpeed;
				ballSpeedY = -ballSpeed;
				ballDirection = 0.25*PI;
				break;
			}
			//reflecBall(colResult, false);
		}

		if (lives <= 0)
		{
			lives = 0;
			isGameOver = true;
		}

		if (isGameOver)
		{
			ballX = -100;
			ballY = -100; //remove ball from screen
			ballSpeedX = 0;
			ballSpeedY = 0; //freeze ball off screen
			for (int i = 0; i < MAX_SPARKLES; i++)
				trail[i].isActive = false;
		}
	}

	//if (input->isKeyDown(aie::INPUT_KEY_LEFT))
	//	paddlePosX -= PADDLE_SPEED * deltaTime;
	//
	//
	//if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
	//	paddlePosX += PADDLE_SPEED * deltaTime;

	////keep the paddle on screen
	//if (paddlePosX - PADDLE_WIDTH/2 < 0)
	//	paddlePosX = PADDLE_WIDTH/2;
	//
	//if (paddlePosX + PADDLE_WIDTH / 2 > getWindowWidth())
	//	paddlePosX = getWindowWidth() - PADDLE_WIDTH /2;


	// wall reflections - disabled to allow for remove image from screen
	//if (ballY > (int)getWindowHeight() - BALL_RADIUS)
	//{
	//	reflecBall(SOUTH, false);
	//	ballY = (int)getWindowHeight() - BALL_RADIUS;
	//}

	//else if (ballY < BALL_RADIUS)
	//{
	//	reflecBall(NORTH, false);
	//	ballY = BALL_RADIUS;
	//}
	//if (ballX > (int)getWindowWidth() - BALL_RADIUS)
	//{
	//	reflecBall(WEST, false);
	//	ballX = (int)getWindowWidth() - BALL_RADIUS;
	//}
	//else if (ballX < BALL_RADIUS)
	//{
	//	reflecBall(EAST, false);
	//	ballX = BALL_RADIUS;
	//}
}

void ShivsGameApp::draw()
{

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	if (!gameStarted && !controlsRaised)
		m_2dRenderer->drawSprite(m_mainMenuTexture, getWindowWidth() / 2, getWindowHeight() / 2, getWindowWidth(), getWindowHeight(), 0, -1);

	if (!gameStarted && controlsRaised)
		m_2dRenderer->drawSprite(m_controlsTexture, getWindowWidth() / 2, getWindowHeight() / 2, getWindowWidth(), getWindowHeight(), 0, -1);

	// draw your stuff here!
	//drawing the paddle
	if (gameStarted)
	{
		m_2dRenderer->drawSprite(m_mirrorsTexture, getWindowWidth() / 2, getWindowHeight() / 2, getWindowWidth(), getWindowHeight(), 0, 0);

		m_2dRenderer->drawSprite(m_backgroundTexture, getWindowWidth() / 2, getWindowHeight() / 2, getWindowWidth(), getWindowHeight(), 0, 3);

		m_2dRenderer->setRenderColour(0, 1, 1);
		//m_2dRenderer->drawBox(mirror[0]->mirrorPosX, mirror[0]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[0]->mirrorRotation);
		//m_2dRenderer->drawBox(mirror[1]->mirrorPosX, mirror[1]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[1]->mirrorRotation);
		//m_2dRenderer->drawBox(mirror[2]->mirrorPosX, mirror[2]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[2]->mirrorRotation);
		//m_2dRenderer->drawBox(mirror[3]->mirrorPosX, mirror[3]->mirrorPosY, MIRROR_WIDTH, MIRROR_HEIGHT, mirror[3]->mirrorRotation);

		//drawing the ball
		m_2dRenderer->setRenderColour(1, 0, 0);
		//m_2dRenderer->drawCircle(ballX, ballY, BALL_RADIUS);
		m_2dRenderer->drawSprite(m_lightTexture, ballX, ballY, BALL_RADIUS, BALL_RADIUS, ballDirection);

		//draw sparkles
		for (int i = 0; i < MAX_SPARKLES; i++)
		{
			if (trail[i].isActive)
				m_2dRenderer->drawCircle(trail[i].x, trail[i].y, SPARKLE_SIZE);
		}

		if (!stageStarted)
			m_2dRenderer->drawText(m_font, "SPACE to start", getWindowWidth() / 2, getWindowHeight() / 2);

		// output some text, uses the last used colour
		m_2dRenderer->setRenderColour(1, 0, 0);
		m_2dRenderer->drawText(m_font, "Press ESC to quit", MIRROR_HEIGHT, MIRROR_HEIGHT);

		char liveNum = '0' + lives;
		char livesText[] = { 'L', 'i', 'v', 'e', 's', ':', ' ', liveNum, '\0' };
		m_2dRenderer->drawText(m_font, livesText, MIRROR_HEIGHT, getWindowHeight() - (MIRROR_HEIGHT + 20));

		if (!stageStarted)
		{
			m_2dRenderer->drawText(m_font, "SPACE to start", getWindowWidth() / 2, getWindowHeight() / 2);
		}

		if (isGameOver)
		{
			m_2dRenderer->setRenderColour(1, 1, 1);
			m_2dRenderer->drawSprite(m_creditsTexture, getWindowWidth() / 2, getWindowHeight() / 2, getWindowWidth()- MIRROR_HEIGHT, getWindowHeight() - MIRROR_HEIGHT, 0, 2);
			m_2dRenderer->setRenderColour(1, 0, 0);
			m_2dRenderer->drawText(m_font, "GAME OVER", getWindowWidth() / 2, getWindowHeight() / 2, 1);
		}


	}

	// done drawing sprites
	m_2dRenderer->end();
}

Direction ShivsGameApp::ballCollideRectangle(int rCentreX, int rCentreY, int rWidth, int rHeight, float rRotation)
{
	//declare first
	int leftSide, rightSide, topSide, bottomSide;
	//additional calculation in case mirror is flipped
	// first calculation is asuming the ball has no area, just a centre point
	if (rRotation > 0)
	{
		leftSide = rCentreX - rHeight / 2 - BALL_RADIUS;
		rightSide = rCentreX + rHeight / 2 + BALL_RADIUS;
		topSide = rCentreY + rWidth / 2 + BALL_RADIUS;
		bottomSide = rCentreY - rWidth / 2 - BALL_RADIUS;
	}
	else {
		leftSide = rCentreX - rWidth / 2 - BALL_RADIUS;
		rightSide = rCentreX + rWidth / 2 + BALL_RADIUS;
		topSide = rCentreY + rHeight / 2 + BALL_RADIUS;
		bottomSide = rCentreY - rHeight / 2 - BALL_RADIUS;
	}

	// hit left or right
	if (ballX >= leftSide && ballX <= rightSide)
	{
		// hit top or botom
		if (ballY >= bottomSide && ballY <= topSide)
		{
			if (lastBallY <= topSide && lastBallY >= bottomSide) // hit from the side
			{
				if (lastBallX < rCentreX) // ball was on the left
				{
					return Direction::WEST;
				}
				else // ball was on the right
				{
					return Direction::EAST;
				}
			}
			else // hit from the top/bottom
			{
				if (lastBallY < rCentreY) // ball was below
				{
					return Direction::SOUTH;
				}
				else // ball was above
				{
					return Direction::NORTH;
				}
			}

			/*if (ballSpeedY > 0 && abs(ballSpeedY) >= abs(ballSpeedX))
			{
				return Direction::SOUTH;
			}
			if (ballSpeedY < 0 && abs(ballSpeedY) <= abs(ballSpeedX))
			{
				return Direction::NORTH;
			}
			if (ballSpeedX > 0 && abs(ballSpeedX) <= abs(ballSpeedY))
			{
				return Direction::WEST;
			}
			if (ballSpeedY < 0 && abs(ballSpeedX) <= abs(ballSpeedX))
			{
				return Direction::EAST;
			}*/
		}
	}

	return Direction::NONE;
}

void ShivsGameApp::reflecBall(Direction colDirection, bool hitPaddle)
{
	// special paddle change styuff
	if (hitPaddle)
	{
		float paddleSideFactor = 0;
		paddleSideFactor = ballX = paddlePosX;
		paddleSideFactor = paddleSideFactor / (MIRROR_WIDTH / 2);
		ballSpeedX = paddleSideFactor * ballSpeed;
	}
	else // simple rectangle reflect
	{
		switch (colDirection)
		{
		case NORTH:
		case SOUTH:
			ballSpeedY = -ballSpeedY;
			break;
		case EAST:
		case WEST:
			ballSpeedX = -ballSpeedX;
			break;
		}
	}
}

bool ShivsGameApp::hasMultiButtonPressed()
{
	aie::Input* input = aie::Input::getInstance();
	//if multiple combinations of buttons are pressed 
	if (input->isKeyDown(aie::INPUT_KEY_W) && input->isKeyDown(aie::INPUT_KEY_A))
		return true;
	if (input->isKeyDown(aie::INPUT_KEY_W) && input->isKeyDown(aie::INPUT_KEY_S))
		return true;
	if (input->isKeyDown(aie::INPUT_KEY_W) && input->isKeyDown(aie::INPUT_KEY_D))
		return true;
	if (input->isKeyDown(aie::INPUT_KEY_A) && input->isKeyDown(aie::INPUT_KEY_S))
		return true;
	if (input->isKeyDown(aie::INPUT_KEY_A) && input->isKeyDown(aie::INPUT_KEY_D))
		return true;
	if (input->isKeyDown(aie::INPUT_KEY_S) && input->isKeyDown(aie::INPUT_KEY_D))
		return true;
	//if it gets through that, return false
	return false;
}

