#pragma once
#include "Application.h"
#include "Renderer2D.h"


#define MIRROR_WIDTH 720
#define MIRROR_HEIGHT 54
#define PI 3.14159

class ShivsGameApp;
class aie::Renderer2D;

class mirrors
{
public:
	mirrors(ShivsGameApp* game);
	~mirrors();

	void update(float deltaTime);
	void draw(aie::Renderer2D* renderer);
	void setStartPaused();
	int mirrorPosX, mirrorPosY;
	float mirrorRotation;

protected:
	ShivsGameApp* game;

	bool isActive;
	bool gameHasPaused;
	//bool hasMouseClick();

};

